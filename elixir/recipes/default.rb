bash "install erlang" do
  code <<-EOS
    wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && sudo dpkg -i erlang-solutions_1.0_all.deb
    sudo apt-get update
  EOS
end

package 'esl-erlang' do
  version "1:19.0"
end

package "elixir" do
  version "1.3.1-1"
end
