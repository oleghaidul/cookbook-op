# Package for curb gem
package "libcurl4-openssl-dev"
# Nodejs package
package "nodejs"
package "npm"

bash "install forever globally" do
  code <<-EOS
    sudo ln -s /usr/bin/nodejs /usr/local/bin/node
    npm install forever -g
  EOS
end
