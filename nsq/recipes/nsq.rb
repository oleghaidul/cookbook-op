include_recipe 'nsq::default'

link "/usr/local/bin/nsqd" do
  to "/tmp/nsq-0.3.8.linux-amd64.go1.6.2/bin/nsqd"
end

template "init.nsqd.conf" do
  path "/etc/init/nsqd.conf"
  source "init.nsqd.conf.erb"
  mode 0644
  owner "root"
  group "root"
end

service 'nsq' do
  provider Chef::Provider::Service::Upstart
  service_name 'nsqd'
  supports :status => true, :restart => true, :reload => true
  action [ :enable, :start ]
end

nginx_web_app 'nsqd'do
  template "nginx.conf.erb"
end
