include_recipe "nginx"

remote_file "/tmp/nsq-0.3.8.linux-amd64.go1.6.2.tar.gz" do
  source "https://s3.amazonaws.com/bitly-downloads/nsq/nsq-0.3.8.linux-amd64.go1.6.2.tar.gz"
end

execute 'unpack nsq' do
  cwd '/tmp/'
  command 'tar -xvzf nsq-0.3.8.linux-amd64.go1.6.2.tar.gz'
end
