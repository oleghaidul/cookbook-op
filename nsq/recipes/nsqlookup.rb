include_recipe 'nsq::default'

link "/usr/local/bin/nsqlookupd" do
  to "/tmp/nsq-0.3.8.linux-amd64.go1.6.2/bin/nsqlookupd"
end

template "init.nsqlookup.conf" do
  path "/etc/init/nsqlookupd.conf"
  source "init.nsqlookupd.conf.erb"
  mode 0644
  owner "root"
  group "root"
end

service 'nsqlookup' do
  provider Chef::Provider::Service::Upstart
  service_name 'nsqlookupd'
  supports :status => true, :restart => true, :reload => true
  action [ :enable, :start ]
end
