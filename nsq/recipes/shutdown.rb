%w[nsq nsqadmin nsqlookup].each do |service_name|
  service service_name do
    provider Chef::Provider::Service::Upstart
    action :stop
  end
end
