include_recipe 'nsq::default'

link "/usr/local/bin/nsqadmin" do
  to "/tmp/nsq-0.3.8.linux-amd64.go1.6.2/bin/nsqadmin"
end

template "init.nsqadmin.conf" do
  path "/etc/init/nsqadmin.conf"
  source "init.nsqadmin.conf.erb"
  mode 0644
  owner "root"
  group "root"
end

service 'nsqadmin' do
  provider Chef::Provider::Service::Upstart
  service_name 'nsqadmin'
  supports :status => true, :restart => true, :reload => true
  action [ :enable, :start ]
end

